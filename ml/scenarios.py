import os

from sklearn.model_selection import train_test_split

import data.data_utils as util
from ml.clusterer import Clusterer
from ml.measurements import Measurer
from ml.pcc import PCC
from ml.performance_calculator import PerformanceCounter

SCENARIO_PATH = os.path.join(util.get_data_path(), "exports", "scenarios")
VMS = ["user", "admin", "share", "dc"]

SCENARIOS = "00550"
GRIDSEARCH = False

n_components = [0.4, 0.5, 0.6, 0.7, 0.8]
out_thresholds = [3.0, 4.5]
fprs = [0.05, 0.075, 0.08, 0.09, 0.1, 0.15]
pcc_only = [(PCC, {
		'n_components': (n_components if GRIDSEARCH else [0.7]),
		'outlier_threshold': (out_thresholds if GRIDSEARCH else [4.5]),
	})]
pcc_only_fpr = [(PCC, {
		'n_components': [0.7],
		'outlier_threshold': ([4.5]),
		'fpr': (fprs if GRIDSEARCH else [0.05])
	})]

cluster_only = [(Clusterer, {
		'min_cluster_size': [0.05],
		'min_samples': [0.6],
		# 'decision_threshold': [0.05],
		'method': (["outlier", "probability", "size", "noise"] if GRIDSEARCH else ["outlier"])
	})]

all_predictors = [*pcc_only, *cluster_only]
PREDICTORS = all_predictors


def scenario1(events: [int]=[None]):
	path = os.path.join(util.get_data_path(), "exports", "machines")
	with open(os.path.join(util.get_data_path(), "imported_persons.csv"), 'r') as mf:
		machines = mf.read().splitlines()

	for machine in machines:
		print(f"Handling {machine}...")
		measurer = Measurer()
		df = util.get_security_dataframe(os.path.join(path, f"{machine}.csv"))
		for event in events:
			if event:
				groups = [df[df['EventID'] == event]]
			else:
				groups = [df[df['EventID'] == 4624]]

			for group in groups:
				train_df, test_df = train_test_split(group, shuffle=False)

				mixed = util.mix_malicious_samples(test_df, event_id=event, mix=False)
				measurer.measure_decision_functions(PREDICTORS, machine, train_df, mixed[0], event)

		print(f"Handled scenario 1 for {machine}.")


def scenario2(events: [int]=[None]):
	path = os.path.join(SCENARIO_PATH, "scenario2")

	pc = PerformanceCounter()
	for machine in VMS:
		print(f"Handling {machine}...")
		measurer = Measurer(pc)
		for event in events:
			train_df = util.get_security_dataframe(os.path.join(path, f"{machine}_train" + (f"_{event}" if event else "") + ".csv"))
			test_df = util.get_security_dataframe(os.path.join(path, f"{machine}_test" + (f"_{event}" if event else "") + ".csv"))
			# test_df = util.get_security_dataframe(os.path.join(path, f"attack_events_{machine}.csv"))
			measurer.measure_roc(PREDICTORS, machine, train_df, test_df, event)

		print(f"Handled scenario 2 for {machine}.")


def validation(events: [int]=[None]):
	path = os.path.join(util.get_data_path(), "exports", "machines")
	with open(os.path.join(util.get_data_path(), "imported_persons.csv"), 'r') as mf:
		machines = mf.read().splitlines()

	pc = PerformanceCounter()
	for machine in machines:  # ["AEhrenstrom", "AEhrenstrom_2", "KAssendelft", "MKerkers", "YVolinsky"]:
		print(f"Handling {machine}...")
		measurer = Measurer(pc)

		df = util.get_security_dataframe(os.path.join(path, f"{machine}.csv"))
		for event in events:
			if event and not isinstance(event, str):
				groups = [df[df['EventID'] == event]]
			else:
				groups = [df[df['EventID'] == 4624]]

			for group in groups:
				train_df, test_df = train_test_split(group)

				if SCENARIOS[2] != "0":
					mixed = util.mix_malicious_samples(test_df, event_id=event, fraction=0.5, sets=int(SCENARIOS[2]))
					measurer.measure_roc(predictors=PREDICTORS, machine=machine, train_df=train_df, test_df=mixed, event=event)

				if SCENARIOS[3] != "0":
					for i in range(int(SCENARIOS[3])):
						mixed = util.mix_malicious_samples(test_df, event_id=event)
						measurer.measure_validation(predictors=PREDICTORS, machine=machine, train_df=train_df, test_df=mixed, event=event)

				if SCENARIOS[4] != "0":
					measurer.measure_validation(predictors=PREDICTORS, machine=machine, train_df=train_df, test_df=[test_df], event=event)

	if SCENARIOS[3] != "0":
		pc.save_performance()

	if SCENARIOS[2] != "0":
		pc.save_overall_roc()
		pc.save_thresholds()

	print(f"Handled scenario 3.")


if __name__ == "__main__":
	event_ids = [None]
	if SCENARIOS[0] != "0":
		print(f"Running scenario 1")
		scenario1(event_ids)

	if SCENARIOS[1] != "0":
		print(f"Running scenario 2")
		scenario2(event_ids)

	if SCENARIOS[2] != "0" or SCENARIOS[3] != "0" or SCENARIOS[4] != "0":
		print(f"Running scenario 3")
		validation(event_ids)
