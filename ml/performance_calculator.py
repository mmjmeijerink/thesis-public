import os

import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc

import data.data_utils as util


class PerformanceCounter:
	def __init__(self):
		self._performance = pd.DataFrame(columns=["machine", "event", "classifier", "fp", "tp", "tn", "fn"])
		self._predictions = pd.DataFrame(columns=["machine", "classifier", "true", "pred", "fold"])
		self._thresholds = pd.DataFrame(columns=["machine", "classifier", "fpr", "tpr", "threshold"])

	@property
	def performance(self):
		return self._performance

	@staticmethod
	def print_performance(fp, tp, tn, fn):
		accuracy, precision, recall, fpr = PerformanceCounter.calculate_measures(fp, tp, tn, fn)

		print(f"Performance:")
		print(f"fp: {fp}")
		print(f"tp: {tp}")
		print(f"tn: {tn}")
		print(f"fn: {fn}")
		print(f"mal ({tp + fn}) + benign ({fp + tn}) = {tp + fn + tn + fn}")
		print()
		print(f"Accuracy:\t{accuracy}")
		print(f"Precision:\t{precision}")
		print(f"Recall:\t\t{recall}")
		print(f"FPR:\t\t{fpr}")

	@staticmethod
	def count_detected_attacks(predictions: np.array, labels: pd.Series):
		events = pd.DataFrame({"prediction": predictions, "label": labels})
		fp = 0
		tp = 0
		tn = 0
		fn = 0

		attack = False
		attack_count = 0
		detected = False
		detected_attacks = 0
		missed_attacks = 0
		misclassifying = False
		misclassified_attacks = 0
		malicious_events = 0
		no_attack_frames = 0
		for index, row in events.iterrows():
			if not attack and row['label'] == 1:
				# Begin of attack
				attack = True
				misclassifying = False

				attack_count += 1
				malicious_events += 1

				# Not when first event == malicious
				if fp + tp + tn + fn > 0:
					no_attack_frames += 1
			elif attack and row['label'] == 0:
				# Register attack classification
				if detected:
					detected_attacks += 1
				else:
					missed_attacks += 1

				# End of attack, reset values
				attack = False
				detected = False
			elif attack and row['label'] == 1:
				malicious_events += 1

			# False positive counting
			if not attack and not misclassifying and row['prediction'] == 1:
				misclassifying = True
				misclassified_attacks += 1

			if not attack and row['prediction'] == 1:
				fp += 1
			elif attack and row['prediction'] == 1:
				tp += 1
				detected = True
			elif not attack and row['prediction'] == 0:
				tn += 1
				misclassifying = False
			elif attack and row['prediction'] == 0:
				fn += 1

		# Count attack if frame ends with attack events
		if attack and detected:
			detected_attacks += 1
		elif attack and not detected:
			missed_attacks += 1
		elif not attack:
			no_attack_frames += 1

		total = attack_count + no_attack_frames + misclassified_attacks + missed_attacks
		accuracy = (detected_attacks + (no_attack_frames - misclassified_attacks)) / total if total > 0 else 0
		precision = detected_attacks / (detected_attacks + misclassified_attacks) if detected_attacks + misclassified_attacks > 0 else 1
		recall = detected_attacks / (detected_attacks + missed_attacks) if attack_count + missed_attacks > 0 else 1

		print("---------------------------")
		print(f"Performance:")
		print(f"Detected:\t\t\t{detected_attacks}/{attack_count}")
		print(f"False positives:\t{misclassified_attacks}")
		print(f"Missed attacks:\t\t{missed_attacks}")
		print()
		print(f"Accuracy:\t\t{accuracy}")
		print(f"Precision:\t\t{precision}")
		print(f"Recall:\t\t\t{recall}")
		print(f"detected + missed == attacks: {detected_attacks + missed_attacks == attack_count}")
		print()

		print("---------------------------")
		print(f"Per event performance:")
		PerformanceCounter.print_performance(fp, tp, tn, fn)
		print("---------------------------")

	@staticmethod
	def count_prediction_performance(predictions: np.array, labels: pd.Series) -> (int, int, int, int):
		events = pd.DataFrame({'predictions': predictions, 'labels': labels})
		fp = len(events[(events['predictions'] == 1) & (events['labels'] == 0)])
		tp = len(events[(events['predictions'] == 1) & (events['labels'] == 1)])
		tn = len(events[(events['predictions'] == 0) & (events['labels'] == 0)])
		fn = len(events[(events['predictions'] == 0) & (events['labels'] == 1)])

		return fp, tp, tn, fn

	@staticmethod
	def calculate_measures(fp, tp, tn, fn) -> (float, float, float):
		accuracy = (tp + tn) / (fp + tp + tn + fn) if fp + tp + tn + fn > 0 else 0
		precision = tp / (fp + tp) if fp + tp > 0 else 1
		recall = tp / (tp + fn) if tp + fn > 0 else 1
		fpr = fp / (fp + tn) if fp + tn > 0 else 0

		return accuracy, precision, recall, fpr

	# def calculate_performance(self, predictions, labels: pd.Series, event: int, threshold: float=0, adjustment: float=0, print_performance: bool=True):
	# 	fp, tp, tn, fn = PerformanceCounter.count_prediction_performance(predictions, labels)
	#
	# 	self.set_performance(fp, tp, tn, fn, event, threshold, adjustment)
	#
	# 	if print_performance:
	# 		PerformanceCounter.print_performance(fp, tp, tn, fn)

	def add_performance(self, fp, tp, tn, fn, event: int, classifier: str):
		if event not in self._performance:
			self._performance[event] = {}

		if classifier not in self._performance[event]:
			print(f"Added {classifier} to performance counter of event {event}.")
			self._performance[event][classifier] = {
				'fp': 0,
				'tp': 0,
				'tn': 0,
				'fn': 0
			}

		self._performance[event][classifier]['fp'] += fp
		self._performance[event][classifier]['tp'] += tp
		self._performance[event][classifier]['tn'] += tn
		self._performance[event][classifier]['fn'] += fn

	def overall_performance(self, print_performance: bool=False):
		# Save to file
		now = datetime.datetime.now()
		filename = os.path.join(util.get_data_path(), "performance_logs", "Total", f"Scenario3_{now.date()}_{now.hour}H{now.minute}M.csv")

		df = pd.DataFrame(columns=["event", "classifier", "fp", "tp", "tn", "fn"])
		for event in self._performance:
			for classifier in self._performance[event]:
				counter = self._performance[event][classifier]
				summary = {
					'event': event,
					'classifier': classifier,
					'fp': counter['fp'],
					'tp': counter['tp'],
					'tn': counter['tn'],
					'fn': counter['fn']
				}
				df = df.append(summary, ignore_index=True)

				if print_performance:
					print(f"Total performance of classifier {classifier} with event {event}:")
					self.print_performance(counter['fp'], counter['tp'], counter['tn'], counter['fn'])
					print("---------------------------")

		df.to_csv(filename, sep=util.SEP, index=False)

	def set_performance(self, fp, tp, tn, fn, machine: str, event: int, classifier: str, threshold: float=None):
		performance = {
			'machine': machine,
			'event': event,
			'classifier': classifier,
			'threshold': threshold,
			'fp': fp,
			'tp': tp,
			'tn': tn,
			'fn': fn
		}

		self._performance = self._performance.append(performance, ignore_index=True)

	def save_performance(self):
		now = datetime.datetime.now()
		self._performance.to_csv(os.path.join(util.get_data_path(), "performance_logs", "Total", f"Scenario3_{now.date()}_{now.hour}H{now.minute}M.csv"), sep=util.SEP, index=False)

	def add_predictions(self, machine, classifier: str, labels, values, fold):
		predictions_dict = {
			'machine': machine,
			'classifier': classifier,
			'true': labels,
			'pred': values,
			'fold': fold
		}

		predictions_df = pd.DataFrame(predictions_dict, columns=["machine", "classifier", "true", "pred", "fold"])
		self._predictions = pd.concat([self._predictions, predictions_df])

	def save_overall_roc(self):
		from itertools import cycle

		# Create a colour code cycler e.g. 'C0', 'C1', etc.
		colour_codes = map("C{}".format, cycle(range(len(self._predictions['classifier'].unique()))))

		plt.figure()
		for classifier, clf_group in self._predictions.groupby("classifier"):
			clf_tprs = []
			clf_aucs = []
			mean_fpr = np.linspace(0, 1, 100)
			clf_color = next(colour_codes)

			for machine, machine_group in clf_group.groupby("machine"):
				tprs = []
				aucs = []
				for fold, fold_group in machine_group.groupby("fold"):
					true = pd.to_numeric(fold_group['true'])
					pred = pd.to_numeric(fold_group['pred'])
					fpr, tpr, thresholds = roc_curve(true, pred)
					# auc_score = auc(fpr, tpr)

					tprs.append(np.interp(mean_fpr, fpr, tpr))
					tprs[-1][0] = 0.0
					roc_auc = auc(fpr, tpr)
					aucs.append(roc_auc)

					# Fold plot
					# plt.plot(fpr, tpr, label=label, alpha=alpha)

				mean_tpr = np.mean(tprs, axis=0)
				clf_tprs.append(mean_tpr)

				mean_tpr[-1] = 1.0
				mean_auc = auc(mean_fpr, mean_tpr)
				clf_aucs.append(mean_auc)

				# Plotting mean of machine
				plt.plot(mean_fpr, mean_tpr, lw=0.5, alpha=.3)

			clf_mean_tpr = np.mean(clf_tprs, axis=0)
			clf_mean_tpr[-1] = 1.0
			clf_mean_auc = auc(mean_fpr, clf_mean_tpr)
			std_auc = np.std(clf_aucs)
			# Plotting mean of classifier
			plt.plot(mean_fpr, clf_mean_tpr, color=clf_color, label=r'Mean ROC %s (AUC = %0.3f $\pm$ %0.3f)' % (classifier, clf_mean_auc, std_auc), lw=2, alpha=.8)

			std_tpr = np.std(clf_tprs, axis=0)
			tprs_upper = np.minimum(clf_mean_tpr + std_tpr, 1)
			tprs_lower = np.maximum(clf_mean_tpr - std_tpr, 0)
			plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color=clf_color, alpha=.2, label=r'%s $\pm$ 1 std. dev.' % classifier)

		plt.plot([0, 1], [0, 1], color='g', linestyle='--', label=f"Random classification")
		plt.xlim([0.0, 1.0])
		plt.ylim([0.0, 1.0])
		plt.xlabel(f"False Positive Rate")
		plt.ylabel(f"True Positive Rate")
		plt.title(f"ROCs per classifier")
		plt.legend(loc="lower right")

		filename = f"Mean_ROCs.pdf"
		plt.savefig(os.path.join(util.get_data_path(), "performance_logs", "ROCs", filename), bbox_inches="tight")

	def add_thresholds(self, machine: str, classifier: str, fpr, tpr, thresholds):
		th_dict = {
			'machine': machine,
			'classifier': classifier,
			'fpr': fpr,
			'tpr': tpr,
			'threshold': thresholds
		}

		threshold_df = pd.DataFrame(th_dict, columns=["machine", "classifier", "fpr", "tpr", "threshold"])
		self._thresholds = pd.concat([self._thresholds, threshold_df])

	def save_thresholds(self):
		now = datetime.datetime.now()
		self._thresholds.to_csv(os.path.join(util.get_data_path(), "performance_logs", "Total", f"ROC_thresholds_{now.date()}_{now.hour}H{now.minute}M.csv"), sep=util.SEP, index=False)
