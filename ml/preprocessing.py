import os
from collections import defaultdict

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler

import data.data_utils as util

VM_USERS_TUPLE = ("admin", "user")  # , "share", "dc")
VM_USERS_NORMAL_TUPLE = ("admin_HHH-Normal", "admin_Normal", "user_HHH-Normal", "user_Normal")


class Preprocessor:
	def __init__(self, dataframe: pd.DataFrame):
		self._original_dataframe = dataframe
		self._processed_df = None
		self._encoder_dict = None

		self._labels_train = None
		self._train = None
		self._labels_test = None
		self._test = None

	@property
	def original_dataframe(self):
		return self._original_dataframe

	@property
	def processed_df(self):
		return self._processed_df

	@property
	def encoder_dict(self) -> defaultdict:
		return self._encoder_dict

	@property
	def labels_train(self):
		return self._labels_train

	@property
	def train(self):
		return self._train

	@property
	def labels_test(self):
		return self._labels_test

	@property
	def test(self):
		return self._test

	@staticmethod
	def label_dataset(dataframe: pd.DataFrame, attack_times: pd.DataFrame) -> pd.DataFrame:
		log_tz = "Europe/London"
		attack_times_tz = "Europe/Amsterdam"

		dataframe.insert(0, "Label", 0)
		for index, attack in attack_times.iterrows():
			person = attack['Person']
			# Account for timezone difference, GMT+2 vs GMT in Windows security log
			start = pd.to_datetime(attack['Start'])  # .tz_localize(attack_times_tz).tz_convert(log_tz)  # - pd.Timedelta("2 hours")
			end = pd.to_datetime(attack['End'])  # .tz_localize(attack_times_tz).tz_convert(log_tz)  # - pd.Timedelta("2 hours")

			# (dataframe['@SystemTime'].dt.tz_localize(log_tz) > start) & (dataframe['@SystemTime'].dt.tz_localize(log_tz) <= end)
			mask = ((dataframe['Person'] == person) | (dataframe['Person'] == "dc")) & (dataframe['@SystemTime'] > start) & (dataframe['@SystemTime'] <= end)
			dataframe.loc[mask, "Label"] = 1

		return dataframe

	@staticmethod
	def filter_noise(dataframe: pd.DataFrame) -> pd.DataFrame:
		# Remove local system only events
		dataframe = Preprocessor.remove_local_system_accounts(dataframe)
		# Filter system users
		dataframe = Preprocessor.filter_system_users(dataframe)

		return dataframe

	@staticmethod
	def remove_local_system_accounts(dataframe: pd.DataFrame):
		if 4624 in dataframe['EventID'].unique():
			dataframe = dataframe[~((dataframe['EventID'] == 4624) & (dataframe['LogonType'] == 5) & (dataframe['SubjectUserSid'] == "S-1-5-18"))]
			# dataframe = dataframe[~((dataframe['EventID'] == 4624) & (dataframe['LogonType'] == 5))]  # Remove service account logons
			dataframe = dataframe[~((dataframe['EventID'] == 4624) & ((dataframe['LogonType'] == 0) | (dataframe['LogonType'] == 3)) & (dataframe['SubjectUserSid'] == "S-1-0-0") & (dataframe['TargetUserSid'] == "S-1-5-18"))]

			# Unlock experiment
			# dataframe = dataframe[~((dataframe['EventID'] == 4624) & (dataframe['LogonType'] == 7))]

		return dataframe

	@staticmethod
	def filter_system_users(dataframe: pd.DataFrame) -> pd.DataFrame:
		dataframe = dataframe[~(
			(dataframe['SubjectUserName'].str.contains("DWM"))
			| (dataframe['SubjectUserName'].str.contains("UMFD"))
			| (dataframe['TargetUserName'].str.contains("DWM"))
			| (dataframe['TargetUserName'].str.contains("UMFD"))
			# | (dataframe['TargetUserName'].str.contains("eotten"))  # Domain Admin
		)]

		return dataframe

	@staticmethod
	def remove_duplicates(dataframe: pd.DataFrame) -> pd.DataFrame:
		removed = len(dataframe.drop_duplicates(['Person', 'EventRecordID'], 'last', inplace=True))
		print(f"Removed {removed} duplicates.")

		return dataframe

	@staticmethod
	def add_time_since_last(dataframe: pd.DataFrame) -> pd.DataFrame:
		dataframe['TimeSinceLastEvent'] = dataframe['@SystemTime'] - dataframe['@SystemTime'].shift()
		dataframe['TimeSinceLastEvent'] = dataframe['TimeSinceLastEvent'].apply(lambda x: x.total_seconds())

		tsle_col_index = dataframe.columns.get_loc('TimeSinceLastEvent')
		for person in dataframe['Person'].unique():
			row = dataframe.index[dataframe['Person'] == person][0]
			dataframe.iat[row, tsle_col_index] = 0

		return dataframe

	@staticmethod
	def remove_non_overlapping_columns(dataframe: pd.DataFrame) -> (pd.DataFrame, list):
		vm_users = dataframe[dataframe['Person'].isin(util.VM_USERS_LIST)]
		non_vm_users = dataframe[~(dataframe['Person'].isin(util.VM_USERS_LIST))]

		dropped = None
		if len(vm_users) > 0 and len(non_vm_users) > 0:
			vm_users_cols = []
			non_vm_users_cols = []
			for column in dataframe.columns:
				if column not in util.SPECIALS:
					if vm_users[column].isna().all():
						vm_users_cols.append(column)

					if non_vm_users[column].isna().all():
						non_vm_users_cols.append(column)

			dropped = set(vm_users_cols).symmetric_difference(set(non_vm_users_cols))
			dataframe = dataframe.drop(columns=dropped)

		return dataframe, dropped

	@staticmethod
	def fill_na(dataframe: pd.DataFrame) -> pd.DataFrame:
		for column in dataframe.columns:
			if dataframe[column].dtype.type is np.object_:
				dataframe[column].fillna("null", inplace=True)
			elif dataframe[column].dtype.type is np.datetime64:
				dataframe[column].fillna(pd.Timestamp(0), inplace=True)
			else:
				dataframe[column].fillna(-1, inplace=True)

		return dataframe

	@staticmethod
	def reverse_na(dataframe: pd.DataFrame) -> pd.DataFrame:
		for column in dataframe.columns:
			if dataframe[column].dtype.type is np.object_:
				dataframe.loc[dataframe[column] == "null", column] = np.nan
			elif dataframe[column].dtype.type is np.datetime64:
				dataframe.loc[dataframe[column] == pd.Timestamp(0), column] = pd.NaT
			else:
				dataframe.loc[dataframe[column] == -1, column] = np.nan

		return dataframe

	def fit_encoding(self, dataframe: pd.DataFrame) -> pd.DataFrame:
		""" Encoding the variable """
		if not self.encoder_dict:
			self._encoder_dict = defaultdict(LabelEncoder)

		return dataframe.apply(lambda x: self._encoder_dict[x.name].fit_transform(x))

	def apply_encoding(self, dataframe: pd.DataFrame) -> pd.DataFrame:
		""" Label future data using the dictionary """
		return dataframe.apply(lambda x: self._encoder_dict[x.name].transform(x))

	def inverse_encoding(self, dataframe: pd.DataFrame, column: str=None) -> pd.DataFrame:
		""" Inverse the encoded """
		if column:
			dataframe = self.encoder_dict[column].inverse_transform(dataframe[column])
		else:
			dataframe = dataframe.apply(lambda x: self.encoder_dict[x.name].inverse_transform(x))

		return dataframe

	def split(self, dataframe: pd.DataFrame, splittime_workstations: np.datetime64=None, splittime_vms: np.datetime64=None, splittime_vms_normal: np.datetime64=None) -> (pd.DataFrame, pd.DataFrame):
		if not splittime_workstations:
			splittime_workstations = pd.to_datetime("2018-07-01")

		if not splittime_vms:
			splittime_vms = pd.to_datetime("2018-07-24 13:45:00")

		if not splittime_vms_normal:
			splittime_vms_normal = pd.to_datetime("2018-09-10")

		if dataframe['Person'].dtype.type == np.int64:
			vm_users = self.encoder_dict['Person'].transform(list(VM_USERS_TUPLE))
			vm_users_normal = self.encoder_dict['Person'].transform(list(VM_USERS_NORMAL_TUPLE))
		else:
			vm_users = dataframe[dataframe['Person'].str.startswith(VM_USERS_TUPLE)]['Person'].unique()
			vm_users_normal = dataframe[dataframe['Person'].str.startswith(VM_USERS_NORMAL_TUPLE)]['Person'].unique()

		# Check whether the dataframe is encoded or not
		if "@SystemTime" not in dataframe:
			if self.original_dataframe['@SystemTime'].dtype.name == "datetime64[ns]":
				time_series = self.original_dataframe['@SystemTime']
			else:
				time_series = self.encoder_dict['@SystemTime'].inverse_transform(self.original_dataframe['@SystemTime'])
		else:
			if dataframe['@SystemTime'].dtype.name == "datetime64[ns]":
				time_series = dataframe['@SystemTime']
			else:
				time_series = self.encoder_dict['@SystemTime'].inverse_transform(dataframe['@SystemTime'])

		train_non_vms = dataframe[~(dataframe['Person'].isin(vm_users)) & (time_series < splittime_workstations)]
		test_non_vms = dataframe[~(dataframe['Person'].isin(vm_users)) & (time_series >= splittime_workstations)]
		train_vms = dataframe[(dataframe['Person'].isin(vm_users)) & (time_series < splittime_vms)]
		test_vms = dataframe[(dataframe['Person'].isin(vm_users)) & (time_series >= splittime_vms)]
		train_vms_normal = dataframe[(dataframe['Person'].isin(vm_users_normal)) & (time_series < splittime_vms_normal)]
		test_vms_normal = dataframe[(dataframe['Person'].isin(vm_users_normal)) & (time_series >= splittime_vms_normal)]

		self._train = pd.concat([train_non_vms, train_vms_normal, train_vms], sort=False)
		self._test = pd.concat([test_non_vms, test_vms_normal, test_vms], sort=False)

		# Separate labels from data
		self._train = self._train
		self._test = self._test

		return self._train, self._test

	def preprocess(self, dataframe: pd.DataFrame, attack_times: pd.DataFrame=None, remove_non_overlapping=False) -> pd.DataFrame:
		self._processed_df = None
		if "Label" not in dataframe.columns:
			if not attack_times:
				attack_times = util.get_attack_times()

			dataframe = self.label_dataset(dataframe, attack_times)

		# Filter noise
		dataframe = self.filter_noise(dataframe)

		# Remove duplicates
		# dataframe = self.remove_duplicates(dataframe)

		# Add seconds since last event
		# dataframe = self.add_time_since_last(dataframe)

		# Remove columns with all nan for either VM or non VM
		if remove_non_overlapping:
			if len(dataframe['Person'].unique()) > 1:
				dataframe, dropped = self.remove_non_overlapping_columns(dataframe)

				# Drop Computer anyway
				dataframe = dataframe.drop(columns=["Computer"])

				if dropped:
					print(f"Dropped {len(dropped)} non overlapping columns:")
					print(dropped)
			else:
				print(f"Not dropping 'non overlapping columns' as only 1 log is available.")

		# Fill all empty cells so dtypes will be inferred correctly
		dataframe = self.fill_na(dataframe)
		# dataframe.to_csv(result_file + ".csv", sep=util.SEP, index=False)
		# Label all discrete values
		dataframe = self.fit_encoding(dataframe)

		self._processed_df = dataframe

		return dataframe

	def reverse_preprocessing(self, dataframe: pd.DataFrame):
		# Inverse the encoding
		dataframe = self.inverse_encoding(dataframe)
		# Replace 'null' placeholders with pandas standard: np.nan
		dataframe = self.reverse_na(dataframe)

		return dataframe


if __name__ == '__main__':
	result_file = os.path.join(util.get_data_path(), "exports", "combined_dataset_labeled")
	dataset_file = os.path.join(util.get_data_path(), "exports", "combined_dataset.csv")

	df = util.get_security_dataframe(dataset_file)
	preprocessor = Preprocessor(df)
	df = preprocessor.preprocess(df)
	df.to_pickle(result_file + ".pickle")
