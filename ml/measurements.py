import os
from typing import Union, Tuple, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import matrix
from sklearn.base import BaseEstimator

from sklearn.metrics import auc, confusion_matrix, roc_curve
from sklearn.model_selection import ParameterGrid

import data.data_utils as util
from ml.clusterer import Clusterer
from ml.feature_selection import Selector
from ml.pcc import PCC
from ml.performance_calculator import PerformanceCounter
from ml.preprocessing import Preprocessor
from ml.translation import translate_labelled_to_ws

PRINT = True


class Measurer:
	def __init__(self, performance_counter: PerformanceCounter=None):
		self._performance_counter = performance_counter
		self._preprocessor = None
		self._selector = None
		self._train_special_cols = None
		self._test_special_cols = None

		self._current_event = None

	@property
	def performance_counter(self):
		return self._performance_counter

	def get_timestampes(self) -> []:
		train_stamps = self._preprocessor.inverse_encoding(self._train_special_cols, "@SystemTime")
		test_stamps = self._preprocessor.inverse_encoding(self._test_special_cols, "@SystemTime")
		timestamps = np.append(train_stamps, test_stamps)

		return timestamps

	def preprocess(self, dataframe: pd.DataFrame) -> pd.DataFrame:
		print("Preprocessing...")
		if not self._preprocessor:
			print("Executing...")
			self._preprocessor = Preprocessor(dataframe)
			dataframe = self._preprocessor.preprocess(dataframe)
		else:
			print("Already preprocessed.")
			dataframe = self._preprocessor.processed_df

		print("Preprocessed")

		return dataframe

	def select(self, dataframe: pd.DataFrame, logon_only: bool=False) -> pd.DataFrame:
		print("Selecting...")
		self._selector = Selector(dataframe, self._preprocessor)
		print(f"Selected for event {self._current_event}:")

		if logon_only:
			dataframe = self._selector.logon_event_fixed_selection(dataframe)
		else:
			dataframe = self._selector.feature_selection(dataframe)

		print(f"{dataframe.columns}")
		print("Selected")

		return dataframe

	def prepare_data(self, train_df: pd.DataFrame, test_dfs: Union[pd.DataFrame, List[pd.DataFrame]], event: int) -> Tuple[pd.DataFrame, pd.Series, List[Tuple[pd.DataFrame, pd.Series]]]:
		self._current_event = event

		if isinstance(test_dfs, list):
			if "Test" not in train_df.columns:
				train_df.insert(0, "Test", 0)

			df = train_df
			for i, test_df in enumerate(test_dfs, 1):
				if "Test" not in test_df.columns:
					test_df.insert(0, "Test", i)

				df = pd.concat([df, test_df], sort=False)
		else:
			train_df.insert(0, "Test", 0)
			test_dfs.insert(0, "Test", 1)
			df = pd.concat([train_df, test_dfs], sort=False)

		# Translate user account -> machine
		translated_df = translate_labelled_to_ws(df, event)

		# Preprocess
		pre_df = self.preprocess(translated_df)

		event = "fixed_selection"
		if event:
			if isinstance(event, str) and event == "fixed_selection":
				# For now only login events
				pre_df = pre_df[pre_df['EventID'] == self._preprocessor.encoder_dict['EventID'].transform([4624])[0]]
				logon_only = True
			else:
				pre_df = pre_df[pre_df['EventID'] == self._preprocessor.encoder_dict['EventID'].transform([event])[0]]
				logon_only = (event == 4624)
		else:
			# For now only login events
			pre_df = pre_df[pre_df['EventID'] == self._preprocessor.encoder_dict['EventID'].transform([4624])[0]]
			logon_only = False

		# for event, group in pre_df.groupby("EventID"):
		# Feature selection
		sel_df = self.select(pre_df, logon_only)

		train_set = sel_df[sel_df['Test'] == 0]
		train_lbls = train_set['Label']
		self._train_special_cols = train_set[util.SPECIALS]
		train_set = train_set.drop(columns=util.SPECIALS)

		test_set_tuples = []
		test_sets = sel_df[sel_df['Test'] != 0]
		for i, test_set in test_sets.groupby("Test"):
			test_lbls = test_set['Label']
			self._test_special_cols = test_set[util.SPECIALS]
			test_set = test_set.drop(columns=util.SPECIALS)

			test_set_tuples.append((test_set, test_lbls))

		return train_set, train_lbls, test_set_tuples

	def plot_decision(self, predictors: [(BaseEstimator, {})], machine, train, test, train_labels, test_labels, event=None):
		for predictor in predictors:
			grid = ParameterGrid(predictor[1])
			for params in grid:
				classifier = predictor[0](**params)

				if isinstance(classifier, Clusterer):
					train = pd.concat([train, test])
					if len(grid) > 1:
						param_str = f"min_cluster_size={params['min_cluster_size']}__min_samples={params['min_samples']}"
				else:
					if len(grid) > 1:
						param_str = f"n_components={params['n_components']}__outlier_th={params['outlier_threshold']}__fpr={params['fpr']}"

				classifier.fit(train, train_labels)

				filename_base = (f"Grid_search_" if len(grid) > 1 else "") + f"Density_{machine}" + (f"_{event}" if event else "") + (f"_{param_str}" if len(grid) > 1 else "")
				filename_base = os.path.join(util.get_data_path(), "performance_logs", "Densities", filename_base)
				classifier.plot_decision_function(train=train, test=test, filename_base=filename_base)

	def plot_rocs(self, predictors: [(BaseEstimator, {})], machine, train, train_labels, test_set_tuples, event=None):
		for predictor in predictors:
			plt.figure()
			grid = ParameterGrid(predictor[1])
			for params in grid:
				tprs = []
				aucs = []
				mean_fpr = np.linspace(0, 1, 100)
				for num, (test, test_labels) in enumerate(test_set_tuples, 1):
					classifier = predictor[0](**params)
					clf_type = type(classifier).__name__

					if isinstance(classifier, Clusterer):
						training = pd.concat([train, test])
					else:
						training = train

					classifier.fit(training, train_labels)
					prediction_basis = classifier.decision_function(test)

					if isinstance(classifier, PCC):
						clf_name = "PCC"
						pred_mean = prediction_basis.mean()
						values = [abs(pred_mean - x.sum()) for x in prediction_basis]
						labels = test_labels
					elif isinstance(classifier, Clusterer):
						clf_name = f"Clusterer" + (f"({classifier.method})" if classifier.method != "outlier" else "")
						values = prediction_basis
						labels = pd.concat([train_labels, test_labels])
					else:
						print("No such classifier available.")

					# try:
					fpr, tpr, thresholds = roc_curve(labels, values)
					auc_score = auc(fpr, tpr)

					if self.performance_counter:
						self.performance_counter.add_predictions(machine, clf_type, labels, values, num)
						self.performance_counter.add_thresholds(machine, clf_type, fpr, tpr, thresholds)

					# print(f"{clf_name} with params: {params}")
					# for fp, tp, threshold in zip(fpr, tpr, thresholds):
					# 	print(f"Threshold {threshold}: {tp}@{fp}")
					# except:
					# 	fpr = tpr = 0
					# 	auc_score = "Contained NaNs?"

					if len(grid) > 1:  # Always run all cluster selection methods?
						label = f"{clf_name}, params: {params} (AUC = {auc_score:0.3f})"
						alpha = 1
					else:
						label = f"{clf_name} set {num} (AUC = {auc_score:0.3f})"
						alpha = 0.3

						tprs.append(np.interp(mean_fpr, fpr, tpr))
						tprs[-1][0] = 0.0
						roc_auc = auc(fpr, tpr)
						aucs.append(roc_auc)

					plt.plot(fpr, tpr, label=label, alpha=alpha)

				mean_tpr = np.mean(tprs, axis=0)
				mean_tpr[-1] = 1.0
				mean_auc = auc(mean_fpr, mean_tpr)
				std_auc = np.std(aucs)
				plt.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC (AUC = %0.3f $\pm$ %0.3f)' % (mean_auc, std_auc), lw=2, alpha=.8)

				std_tpr = np.std(tprs, axis=0)
				tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
				tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
				plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev. ')

			# split = max(len(test_labels[test_labels == 0]), len(test_labels[test_labels == 1]))/len(test_labels)
			# plt.plot([0, 1-(split-0.5)], [split-0.5, 1], color='b', linestyle='--', label=f"Naive classification")
			plt.plot([0, 1], [0, 1], color='g', linestyle='--', label=f"Random classification")
			plt.xlim([0.0, 1.0])
			plt.ylim([0.0, 1.0])
			plt.xlabel(f"False Positive Rate")
			plt.ylabel(f"True Positive Rate")
			plt.title(f"ROCs for {machine}")

			if len(grid) > 1:
				loc = (1.02, 0)
			else:
				loc = "lower right"

			plt.legend(loc=loc)

			filename = (f"Grid_search_" if len(grid) > 1 else "") + f"ROC_{clf_name}_{machine}" + (f"_{event}" if event else "") + ".pdf"
			plt.savefig(os.path.join(util.get_data_path(), "performance_logs", "ROCs", filename), bbox_inches="tight")

	def detection_performance(self, predictors: [(BaseEstimator, {})], machine, train, test, train_labels, test_labels, event=None):
		if self.performance_counter:
			pc = self.performance_counter
		else:
			pc = PerformanceCounter()

		# df = df.reset_index(drop=True)
		for predictor in predictors:
			grid = ParameterGrid(predictor[1])
			for params in grid:
				classifier = predictor[0](**params)
				clf_name = type(classifier).__name__

				if isinstance(classifier, Clusterer):
					training = pd.concat([train, test])
					labels = pd.concat([train_labels, test_labels])
				else:
					training = train
					labels = test_labels

				classifier.fit(training, train_labels)
				predictions = classifier.predict(test)

				tn, fp, fn, tp = confusion_matrix(labels, predictions, labels=[0, 1]).ravel()
				pc.print_performance(fp=fp, tp=tp, tn=tn, fn=fn)
				pc.set_performance(fp=fp, tp=tp, tn=tn, fn=fn, machine=machine, event=event, classifier=clf_name, threshold=classifier.prediction_threshold)

				# predicted_frame = pd.concat([train, test])
				# predicted_frame = self._preprocessor.inverse_encoding(predicted_frame)
				# predicted_frame['Prediction'] = ([-1] * len(train) + predictions if len(predictions) < len(predicted_frame) else predictions)
				# filename = f"{machine}_{clf_name}" + (f"_{event}" if event else "") + ".csv"
				# predicted_frame.to_csv(os.path.join(util.get_data_path(), "performance_logs", "Validation", f"{machine}", filename), sep=util.SEP, index=False)

	def measure_decision_functions(self, predictors: [], machine: str, train_df: pd.DataFrame, test_df: pd.DataFrame, event: int=None):
		# Expecting only 1 test_df
		train_set, train_lbls, test_set_tuple = self.prepare_data(train_df, test_df, event)
		self.plot_decision(predictors, machine, train_set, test_set_tuple[0][0], train_lbls, test_set_tuple[0][1], event)

	def measure_roc(self, predictors: [], machine: str, train_df: pd.DataFrame, test_df: List[pd.DataFrame], event: int=None):
		# Expecting list of or single test_df
		train_set, train_lbls, test_set_tuple = self.prepare_data(train_df, test_df, event)
		self.plot_rocs(predictors, machine, train_set, train_lbls, test_set_tuple, event)

	def measure_validation(self, predictors: [], machine: str, train_df: pd.DataFrame, test_df: List[pd.DataFrame], event: int=None):
		# Expecting only 1 test_df
		train_set, train_lbls, test_set_tuple = self.prepare_data(train_df, test_df, event)
		self.detection_performance(predictors, machine, train_set, test_set_tuple[0][0], train_lbls, test_set_tuple[0][1], event)
