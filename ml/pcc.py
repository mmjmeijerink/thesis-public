import numpy as np

from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_is_fitted
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale


class PCC(BaseEstimator):
	def __init__(self, outlier_threshold=4.5, n_components=0.7, threshold=3.70, calculate_full_pca: bool=False):
		self.threshold = threshold
		self.outlier_threshold = outlier_threshold
		self.n_components = n_components
		self.calculate_full_pca = calculate_full_pca

	@property
	def prediction_threshold(self):
		return self.threshold

	def fit(self, X, y=None):
		train_data = scale(X)

		if self.calculate_full_pca:
			self.full_pca_ = PCA()
			self.full_pca_.fit(train_data)

		self.pca_out_ = PCA(n_components=self.n_components)
		self.pca_out_.fit(train_data)

		# Residuals calculation for training data
		scores_out = self.pca_out_.transform(train_data)
		inverse_out = self.pca_out_.inverse_transform(scores_out)
		residuals_out = train_data - inverse_out

		# Remove outliers based on high residual values
		self.outliers_ = self._detect_outliers(residuals_out)
		train_data = np.delete(train_data, self.outliers_, axis=0)

		if len(train_data) < 10:
			print(f"Too little samples left")
			return self
		# else:
			# print(f"Removed {len(self.outliers_)}/{len(residuals_out)} as outlier.")

		# Recalculate PCA after removal of the outliers
		self.pca_ = PCA(n_components=self.n_components)
		self.pca_.fit(train_data)

		# Save mean and std
		scores = self.pca_.transform(train_data)
		inverse = self.pca_.inverse_transform(scores)
		residuals = train_data - inverse

		self.mean_ = residuals.mean()
		self.std_ = residuals.std()

		return self

	def predict(self, X):
		residuals = self.decision_function(X)

		mean = residuals.mean()
		std = residuals.std()
		# predictions = [self._predict(x, mean, std) for x in residuals]

		return [0 if abs(mean - x.sum()) < std * self.threshold else 1 for x in residuals]

	def decision_function(self, X):
		check_is_fitted(self, ["pca_", "mean_", "std_"])

		test_data = scale(X)
		scores = self.pca_.transform(test_data)
		inverse = self.pca_.inverse_transform(scores)
		residuals = test_data - inverse

		return residuals

	def _detect_outliers(self, residuals):
		mean = residuals.mean()
		std = residuals.std()

		outliers = []
		for index, row in enumerate(residuals):
			if abs(mean - row.sum()) > std * self.outlier_threshold:  # (self.threshold + self.adjustment):
				outliers.append(index)

		return outliers

	def plot_decision_function(self, train, test, filename_base: str):
		import matplotlib.pyplot as plt
		import seaborn as sns

		# train, validation = np.array_split(train, 2)

		plt.figure()
		plt.title(f"Residuals density")
		ax = plt.gca()

		residuals_train = self.decision_function(train)
		# residuals_val = self.decision_function(validation)
		residuals_test = self.decision_function(test)

		train_mean = residuals_train.mean()
		test_mean = residuals_test.mean()

		res_train = [abs(train_mean - x.sum()) for x in residuals_train]
		# res_val = [abs(residuals_val.mean() - x.sum()) for x in residuals_val]
		res_test = [abs(test_mean - x.sum()) for x in residuals_test]

		clip = min(10, max(res_train), max(res_test))
		kde_args = {'bw': 0.05, 'clip': (0, clip)}
		sns.distplot(res_train, ax=ax, hist=True, kde=True, label=f"Train (mean = {np.mean(res_train):0.4f}, std = {np.std(res_train):0.4f})", kde_kws=kde_args)
		# sns.distplot(res_val, ax=ax, hist=True, kde=True, label=f"Validation (mean = {np.mean(res_val):0.4f}, std = {np.std(res_val):0.4f})", kde_kws=kde_args)
		sns.distplot(res_test, ax=ax, hist=True, kde=True, label=f"Test (mean = {np.mean(res_test):0.4f}, std = {np.std(res_test):0.4f})", kde_kws=kde_args)

		# Switch train and validation set
		# residuals_train = self.decision_function(validation)
		# residuals_val = self.decision_function(train)
		# residuals_test = self.decision_function(test)
		#
		# res_train = [abs(residuals_train.mean() - x.sum()) for x in residuals_train]
		# res_val = [abs(residuals_val.mean() - x.sum()) for x in residuals_val]
		# res_test = [abs(residuals_test.mean() - x.sum()) for x in residuals_test]
		#
		# sns.distplot(res_train, ax=ax, hist=True, kde=True, label=f"Train 2 (mean = {np.mean(res_train):0.4f}, std = {np.std(res_train):0.4f})", kde_kws={'bw': 0.05, 'clip': (0.0, 10)})
		# sns.distplot(res_val, ax=ax, hist=True, kde=True, label=f"Validation 2 (mean = {np.mean(res_val):0.4f}, std = {np.std(res_val):0.4f})", kde_kws={'bw': 0.05, 'clip': (0.0, 10)})
		# sns.distplot(res_test, ax=ax, hist=True, kde=True, label=f"Test 2 (mean = {np.mean(res_test):0.4f}, std = {np.std(res_test):0.4f})", kde_kws={'bw': 0.05, 'clip': (0.0, 10)})

		plt.xlabel(f"Residual value")
		plt.ylabel(f"Density")
		plt.xlim(0, clip)
		plt.ylim(0, 2)
		plt.legend(loc="upper right")

		filename = filename_base + "_pcc_decision.pdf"
		plt.savefig(filename, bbox_inches="tight")
		plt.close()

	def plot_residuals_over_time(self, train, test, path: str, filename_base: str, timestamps: []=None):
		import os
		import matplotlib.pyplot as plt

		# Prepare timeline plot
		residuals_train = self.decision_function(train)
		residuals_test = self.decision_function(test)

		# res_train = [residuals_train.mean() - x.sum() for x in residuals_train]
		# res_test = [residuals_test.mean() - x.sum() for x in residuals_test]
		# all_residuals = np.concatenate((res_train, res_test))

		fig, ax = plt.figure()
		ax.set_ylim(max(min(20, max(max(residuals_train), max(residuals_test))), 5))
		if timestamps:
			ax.set_xticklabels(timestamps, rot=90)

		for dim in range(len(residuals_test)):
			ax.plot(np.concatenate((residuals_train[dim], residuals_test[dim])))

		filename = os.path.join(path, filename_base + "_pcc_residuals.pdf")
		plt.savefig(filename)  # , bbox_inches="tight")

# ROC plot voor multiple thresholds
# components, thresholds, outlier_thresholds = self.get_pcc_parameters()
# param_grid = {
# 	# 'pcc__component': components,
# 	# 'pcc__threshold': thresholds,
# 	'pcc__outlier_threshold': outlier_thresholds
# }
#
# best_grid = {}
# best_score = 3 * [float("-inf")]
#
# # Prepare plot
# fig = plt.figure(figsize=(7.5, 7.5))
# pc = PerformanceCounter()
# plt_id = 0
# for g in ParameterGrid(param_grid):
# 	plt_id += 1
# 	out_th = g['pcc__outlier_threshold']
# 	pipeline.set_params(**g)
# 	pipeline.fit(train, train_labels)
#
# 	pred_train = pipeline.decision_function(train)
# 	pred_test = pipeline.decision_function(test)
#
# 	res_train = [abs(pred_train.mean() - x.sum()) for x in pred_train]
# 	residuals = [abs(pred_test.mean() - x.sum()) for x in pred_test]
#
# 	# train_fpr, train_tpr, train_curve_thresholds = roc_curve(train_labels, res_train)
# 	fpr, tpr, test_curve_thresholds = roc_curve(test_labels, residuals)
#
# 	print(f"Outlier threshold: {out_th}")
# 	for th in [*test_curve_thresholds, *thresholds]:
# 		pipeline.set_params(**{'pcc__threshold': th})
# 		pred_cls = pipeline.predict(test)
# 		tn, fp, fn, tp = confusion_matrix(test_labels, pred_cls, labels=[0, 1]).ravel()
#
# 		pc.set_performance(fp, tp, tn, fn, 4624, th, out_th)
# 		if PRINT:
# 			print(f"Threshold: {th}")
# 			PerformanceCounter.print_performance(fp, tp, tn, fn)
# 			print("-----------------------------")
# 			print()
#
# 	print(f"End of Outlier threshold: {out_th}")
#
# 	auc_score = auc(fpr, tpr)
# 	plt.plot(fpr, tpr, label=f"ROC outlier th = {out_th} (auc = {auc_score})")
#
# 	# Show results
# 	print(f"auc: {auc_score}")
# 	print("")
#
# Update the best_score if needed, ROC AUC
# if auc_score > best_score[len(best_score) - 1] and auc_score not in best_score:
# 	best_score[len(best_score) - 1] = auc_score
# 	best_score.sort(reverse=True)
# 	best_grid[auc_score] = []
#
# if auc_score in best_score:
# 	best_grid[auc_score].append(g)

# print("Best parameters")
# for score in best_score:
# 	if score != float("-inf"):
# 		print(f"{score}: {best_grid[score]}")
