import os
import re
from typing import Dict, Union

import pandas as pd
from scipy.stats._multivariate import special_ortho_group_frozen, unitary_group_gen
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler

import data.data_utils as util
from ml.mutual_info import calculate_mutual_info_per_event
from ml.preprocessing import Preprocessor


class Selector:
	def __init__(self, dataframe: pd.DataFrame, preprocessor: Preprocessor = None):
		if not preprocessor:
			preprocessor = Preprocessor(dataframe)
			dataframe = preprocessor.preprocess(dataframe)

		self._preprocessor = preprocessor
		self._preprocessed_dataframe = dataframe

		self._zero_variance_selector = None
		self._dropped_columns = {}

	@property
	def preprocessor(self) -> Preprocessor:
		return self._preprocessor

	@property
	def preprocessed_dataframe(self) -> pd.DataFrame:
		return self._preprocessed_dataframe

	@property
	def zero_variance_selector(self) -> VarianceThreshold:
		return self._zero_variance_selector

	@property
	def dropped_columns(self) -> Dict:
		return self._dropped_columns

	def remove_zero_variance(self, dataframe: pd.DataFrame) -> (pd.DataFrame, list):
		if "Label" in dataframe.columns:
			label = dataframe['Label']

		if "Person" in dataframe.columns:
			person = dataframe['Person']

		# Remove zero variance columns, but retain Label no matter what
		self._zero_variance_selector = VarianceThreshold()
		self._zero_variance_selector.fit_transform(dataframe)

		dropped = dataframe.iloc[:, ~dataframe.columns.isin(dataframe.columns[self._zero_variance_selector.get_support(indices=True)])].columns
		dataframe = dataframe.iloc[:, self._zero_variance_selector.get_support()]

		if "Label" in dropped:
			dataframe.insert(0, "Label", label)
			dropped.drop("Label")

		if "Person" in dropped:
			dataframe.insert(0, "Person", person)
			dropped.drop("Person")

		return dataframe, dropped

	def per_event_dissection(self):
		# Per event investigation
		event_investigation(self._preprocessed_dataframe, self._preprocessor)

	def load_mi_scores(self, file=None):
		# Precalculated mutual information scores
		if not file:
			file = os.path.join(util.get_data_path(), "ami_scores", "mutual_info", "mi_score_per_column_complete.csv")

		mi_per_feature = pd.read_csv(file, sep=util.SEP)
		return mi_per_feature

	@staticmethod
	def remove_identifying_columns(dataframe: pd.DataFrame) -> (pd.DataFrame, list):
		drop_columns = []
		for column in dataframe.columns:
			if column not in util.SPECIALS:
				if len(dataframe[column].unique()) / len(dataframe) > 0.95:
					drop_columns.append(column)
				elif column.endswith("ID") or column.endswith("Id") or column == "LogonGuid":  # in ["@SystemTime", "EventRecordID"]:
					drop_columns.append(column)
				elif column == "Computer":
					dataframe = dataframe.drop(columns=["Computer"])

		dataframe = dataframe.drop(drop_columns, axis=1)

		return dataframe, drop_columns

	@staticmethod
	def retrieve_ngrams(dataframe: pd.DataFrame, length: int) -> pd.DataFrame:
		ngram_frame = pd.DataFrame()
		for col in dataframe.columns:
			ngram_frame[col] = dataframe[col].apply(lambda i: util.SEP.join(map(str, dataframe[col].iloc[i:i + length])))

		return ngram_frame

	@staticmethod
	def pca(dataframe: pd.DataFrame, n_components: Union[int, float], with_mean: bool=True) -> (PCA, pd.DataFrame):
		if "Label" in dataframe.columns:
			dataframe = dataframe.drop("Label", axis=1)

		scaler = StandardScaler(with_mean=with_mean)
		dataframe = scaler.fit_transform(dataframe)

		pca = PCA(n_components=n_components)
		dataframe = pca.fit_transform(dataframe)

		return pca, pd.DataFrame(dataframe)

	@staticmethod
	def logon_event_fixed_selection(dataframe: pd.DataFrame) -> pd.DataFrame:
		special_cols = dataframe[util.SPECIALS]
		dataframe = dataframe.drop(columns=util.SPECIALS)

		# cols = ["AuthenticationPackageName", "ElevatedToken", "ImpersonationLevel", "IpAddress", "IpPort", "KeyLength", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "RestrictedAdminMode", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetOutboundDomainName", "TargetOutboundUserName", "TargetUserName", "TargetUserSid", "VirtualAccount", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "ElevatedToken", "ImpersonationLevel", "IpAddress", "IpPort", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetOutboundDomainName", "TargetOutboundUserName", "TargetUserName", "TargetUserSid", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "IpAddress", "IpPort", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]

		# cols = ["AuthenticationPackageName", "ElevatedToken", "IpAddress", "IpPort", "KeyLength", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "ImpersonationLevel", "IpAddress", "IpPort", "KeyLength", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "ElevatedToken", "ImpersonationLevel", "IpAddress", "IpPort", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "ElevatedToken", "IpAddress", "IpPort", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]
		# cols = ["AuthenticationPackageName", "ImpersonationLevel", "IpAddress", "IpPort", "LmPackageName", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetUserName", "TargetUserSid", "TargetOutboundDomainName", "TargetOutboundUserName", "WorkstationName"]
		cols = ["ElevatedToken", "ImpersonationLevel", "IpAddress", "IpPort", "KeyLength", "LogonProcessName", "LogonType", "ProcessName", "SubjectDomainName", "SubjectUserName", "SubjectUserSid", "TargetDomainName", "TargetOutboundDomainName", "TargetOutboundUserName", "TargetUserName", "TargetUserSid", "WorkstationName"]
		dataframe = dataframe[cols]

		dataframe = pd.concat([special_cols, dataframe], axis=1)

		return dataframe
	
	def feature_selection(self, dataframe: pd.DataFrame, per_event: bool=False) -> Union[pd.DataFrame, Dict[str, pd.DataFrame]]:
		special_cols = dataframe[util.SPECIALS]
		dataframe = dataframe.drop(columns=util.SPECIALS)

		# if per_event:
		# 	event_dfs = {}
		# 	for event, group in dataframe.groupby("EventID"):
		# 		try:
		# 			event_df = self.feature_selection(group, False)
		# 			event_df = event_df.insert(0, util.SPECIALS, special_cols[group.index])
		# 			event_dfs[event] = event_df
		# 		except ValueError:
		# 			if event in event_dfs:
		# 				print("Should be here")
		#
		# 	return event_dfs
		# else:
		dataframe, zero_variance = self.remove_zero_variance(dataframe)
		dataframe, identifying = self.remove_identifying_columns(dataframe)

		self._dropped_columns['zero_variance'] = zero_variance
		self._dropped_columns['identifying'] = identifying

		dataframe = pd.concat([special_cols, dataframe], axis=1)

		return dataframe


def event_investigation(dataframe: pd.DataFrame, pp: Preprocessor):
	vm_users = pp.encoder_dict['Person'].transform(["user", "admin", "share", "dc"])
	non_vm_users = dataframe[~(dataframe['Person'].isin(vm_users))]['Person'].unique()
	for event_id, group in dataframe.groupby("EventID", as_index=False):
		if len(group) > 500 or (len(set(group['Person'].unique()).intersection(non_vm_users)) > 0 and len(set(group['Person'].unique()).intersection(vm_users)) > 0):
			if len(group) < 10:
				print(group)
			else:
				event_id_str = pp.encoder_dict['EventID'].inverse_transform(event_id)

				zv = VarianceThreshold()
				zv.fit_transform(group)
				event = group.iloc[:, zv.get_support()]
				event = pp.reverse_preprocessing(event)

				event_file = os.path.join(util.get_data_path(), "exports", "combined", f"Combined_event_{event_id_str}.csv")
				event.to_csv(event_file, sep=util.SEP)

				header_description_file = os.path.join(util.get_data_path(), "Event_headers", "Summaries", f"Event_{event_id_str}.csv")
				with open(header_description_file, 'w+') as f:
					f.write(str(len(event)) + util.SEP + util.SEP.join(event.columns) + "\n")

					unique_counts = [len(event[column].unique()) for column in event.columns]
					f.write(f"Unique values{util.SEP}{util.SEP.join(map(str, unique_counts))}\n")


def mi_per_event():
	path = os.path.join(util.get_data_path(), "exports", "combined")
	template = r"Combined_event_(\d*).csv"
	files = os.listdir(path)
	event_files = list(zip([re.match(template, file).group(1) for file in files], files))
	for event_id, event_file in event_files:
		event_df = util.get_security_dataframe(os.path.join(path, event_file))

		preprocessor = Preprocessor(event_df)
		event_df = preprocessor.preprocess(event_df)

		calculate_mutual_info_per_event(event_id, event_df)


if __name__ == '__main__':
	result_file = os.path.join(util.get_data_path(), "exports", "combined_dataset_selected.pickle")
	dataset_file = os.path.join(util.get_data_path(), "exports", "combined_dataset.csv")
	# dataset_file = os.path.join(util.get_data_path(), "exports", "2018-07-24_2018-07-24_VMs_log_timeframe.csv")
	# dataset_file = os.path.join(util.get_data_path(), "exports", "combined_dataset_labeled.pickle")
	# !!!dataset_file = os.path.join(util.get_data_path(), "exports", "servers")
	df = util.get_security_dataframe(dataset_file)

	selector = Selector(df)
	df = selector.feature_selection(selector.preprocessed_dataframe)
	df.to_pickle(result_file)

	print(f"Dropped zero variance columns:\t{selector.dropped_columns['zero_variance']}")
	print(f"Dropped identifying columns:\t{selector.dropped_columns['identifying']}")

# (A)MI Calculations
# calculate_mutual_info(df)

# Per event relevant features MI calculation
# mi_per_event()
