from typing import Union

import hdbscan
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_is_fitted


class Clusterer(BaseEstimator):
	"""
	Methods supported:
		- outlier: Outlier score above decision_threshold
		- probability: Probability score above decision_threshold (How about noise?)
		- size: Cluster size (/density?) below decision_threshold
		- noise: Points classified as noise, decision_threshold ignored
	"""
	def __init__(self, min_cluster_size: Union[int, float]=0.05, min_samples: Union[int, float]=25, decision_threshold: float=0.007, method: str="outlier"):  # Old threshold: 0.0934
		self.min_cluster_size = min_cluster_size
		self.min_samples = min_samples
		self.decision_threshold = decision_threshold
		self.method = method.lower()

	@property
	def prediction_threshold(self):
		return self.decision_threshold

	def fit(self, X, y=None):
		if isinstance(self.min_cluster_size, float):
			cluster_size = int(len(X) * self.min_cluster_size)
		else:
			cluster_size = self.min_cluster_size

		if isinstance(self.min_samples, float):
			samples = int(cluster_size * self.min_samples)
		else:
			samples = self.min_samples

		samples = max(samples, 30)
		cluster_size = max(cluster_size, int(1.5 * samples))
		# print(f"Cluster size: {cluster_size} and samples: {samples}")

		self.clusterer_ = hdbscan.HDBSCAN(min_cluster_size=cluster_size, min_samples=samples)
		self.clusterer_.fit(X)

		self.train_ = X[:len(y)]
		self.test_ = X[len(y):]

		return self

	def predict(self, X=None):
		decision_basis = self.decision_function(X)

		if self.method == "outlier":
			predictions = [0 if not np.isnan(value) and value < self.decision_threshold else 1 for value in decision_basis]
		elif self.method == "probability":
			predictions = [0 if value > self.decision_threshold else 1 for value in decision_basis]
		elif self.method == "size":
			sizes = []
			size_of_label = {}
			for label, size in np.unique(self.clusterer_.labels_, return_counts=True):
				size_of_label[label] = size
				sizes.append(size)

			predictions = [0 if size_of_label[label] > self.decision_threshold * np.mean(sizes) else 1 for label in decision_basis]
		elif self.method == "noise":
			predictions = [0 if value != -1 else 1 for value in decision_basis]

		return predictions

	def decision_function(self, X=None):
		check_is_fitted(self, ["clusterer_", "train_", "test_"])

		if len(X) == len(self.train_):
			decision_slice = slice(0, len(self.train_))
		else:
			decision_slice = slice(0, None)  # len(self.train_), None)

		if self.method == "outlier":
			decision_basis = self.clusterer_.outlier_scores_  # [decision_slice]
		elif self.method == "probability":
			decision_basis = self.clusterer_.probabilities_[decision_slice]
		elif self.method == "size" or self.method == "noise":
			decision_basis = self.clusterer_.labels_[decision_slice]
		else:
			print(f"Method '{self.method}' not supported.")

		decision_basis = [0 if np.isnan(value) else value for value in decision_basis]

		return decision_basis

	def plot_decision_function(self, train, test, filename_base: str):
		import matplotlib.pyplot as plt
		import seaborn as sns

		plt.figure()
		plt.title(f"Decision basis values")
		ax = plt.gca()

		test_decision = self.decision_function(self.test_)

		if self.method in ["size", "noise"]:
			train_decision = self.decision_function(self.train_)
		else:
			train_decision = test_decision[:len(test_decision) - len(test)]
			test_decision = test_decision[len(test_decision) - len(test):]

		train_scores = train_decision[np.isfinite(train_decision)]
		test_scores = test_decision[np.isfinite(test_decision)]

		kde_args = {
			'bw': 0.5,
			'clip': ((-1 if self.method in ["size", "noise"] else 0.0), max(1.0, max(test_scores)))
		}
		sns.distplot(train_scores, ax=ax, hist=True, kde=True, label=f"Train\t(mean = {np.mean(train_scores):0.4f}, std = {np.std(train_scores):0.4f})", kde_kws=kde_args)
		sns.distplot(test_scores, ax=ax, hist=True, kde=True, label=f"Test\t(mean = {np.mean(test_scores):0.4f}, std = {np.std(test_scores):0.4f})", kde_kws=kde_args)

		plt.xlabel(f"{self.method} value")
		plt.ylabel(f"Density")
		plt.legend(loc="upper right")

		filename = filename_base + f"_clusterer_{self.method}_decision.pdf"
		plt.savefig(filename, bbox_inches="tight")
		plt.close()
