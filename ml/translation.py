import pandas as pd


def construct_replacement_dict(computer, user, sid):
	replacement_dict = {
		# REDACTED
	}

	return replacement_dict


def retrieve_ws_user_sid(dataframe: pd.DataFrame, event: int=None) -> (str, str, str):
	if not event:
		event = 4624

	df = dataframe[(dataframe['EventID'] == event)]
	df = df[(df['SubjectDomainName'] == "NL") & (df['TargetDomainName'] == "NL") & (df['SubjectUserSid'] == "S-1-5-18")]

	computers = df['SubjectUserName'].unique()
	if len(computers) != 1:
		print(f"Not one unique computer found!")
		print(f"Computers: {computers}")

	target_ws = computers[0]
	local_users = df['TargetUserName'].unique()
	if len(local_users) != 1:
		print(f"Not one unique local domain account found!")
		print(f"Users: {local_users}")

	target_user = local_users[0]
	sids = df['TargetUserSid'].unique()
	if len(sids) != 1:
		print(f"Not one unique sid found given target ws and target user!")
		print(f"Sids: {sids}")

	sid = sids[0]

	return target_ws, target_user, sid


def translate_labelled_to_ws(dataframe: pd.DataFrame, event: int=None) -> pd.DataFrame:
	target_ws, target_user, sid = retrieve_ws_user_sid(dataframe, event)
	replacement_dict = construct_replacement_dict(target_ws, target_user, sid)
	replacing = dataframe.replace(replacement_dict)

	return replacing
