from collections import OrderedDict
import json
import os
import re
from typing import Union

import xmltodict

from data.data_utils import get_data_path

SEP = "|"


def write_xml_to_file(person, events, index: str, header_filename="headers.csv", post_fix="_combined.csv"):
	path = os.path.join(get_data_path(), "exports")
	filename = index + post_fix
	file = os.path.join(path, filename)
	file_existed = os.path.isfile(file)

	headers_from_file = []
	header_file = os.path.join(get_data_path(), header_filename)
	with open(header_file, 'r') as hf:
		headers_from_file = hf.readline()

	headers = headers_from_file.split(SEP)
	write_xml_rows(person, events, file, headers)

	header_row = SEP.join(headers)
	if headers_from_file != header_row or not file_existed:
		print("Headers changed (or new file):")
		print(f"headers.csv:\t{headers_from_file.split(SEP)}")
		print(f"headers list:\t{headers}")

		# Add new header to export file
		prepend_header(file, path, header_row, file_existed)

		# Save header row in header file
		with open(header_file, 'w+') as hf:
			hf.write(header_row)


def prepend_header(file, path, header, file_existed):
	tmp_file = os.path.join(path, "newfile.txt")
	with open(file, 'r') as f:
		with open(tmp_file, 'w') as tmp:
			tmp.write(header + "\n")
			if file_existed:
				f.readline()  # Skip old header

			for line in f:
				tmp.write(line)

	os.replace(tmp_file, file)


def write_xml_rows(person, events, file, headers):
	with open(file, 'a+') as f:
		for hit in events:
			if isinstance(hit, str):
				event = hit.lstrip("{")
				i = event.find("{")
				if i > 0:
					event = json.loads(event[i:])
				else:
					continue
			else:
				event = hit

			if "xml_string" in event:
				xmldict = xmltodict.parse(event['xml_string'])

				determine_headers(xmldict, headers)
				row_str = parse_row(person, xmldict, headers)

				if not row_str:
					continue

				# Handle weird characters
				row_str = str(row_str.encode("utf-8")).lstrip("b'").rstrip("'")
				f.write(row_str + "\n")


def determine_headers(xmldict, headers):
	h = []
	event = xmldict['Event']

	if "System" in event:
		system = event['System']
		for key in system.keys():
			if isinstance(system[key], OrderedDict):
				h += system[key].keys()
			elif system[key]:
				h.append(key)

	if "EventData" in event:
		event_data = event['EventData']
		if event_data:
			if "Data" in event_data:
				data = event_data['Data']
				if isinstance(data, list):
					for item in data:
						if item and isinstance(item, dict):
							if "@Name" in item:
								h.append(item['@Name'])
						elif item:
							print(f"Found unknown header type: {item}")
				elif isinstance(data, OrderedDict):
					if "@Name" in data:
						h.append(data['@Name'])
					else:
						print(f"Unknown structure of EventData.Data: {data}")
				else:
					print(f"Unknown instance of EventData.Data: {data}")

			if len(event_data) > 1:
				print(f"Found unknown values in {event_data}")

	diff = list(set(h) - set(headers))
	headers += diff


def parse_row(person, xmldict, headers) -> Union[str, None]:
	row = {headers[0]: person}
	event = xmldict['Event']

	if "System" in event:
		system = event['System']
		for key in system.keys():
			if key in headers:
				if system[key] and not (isinstance(system[key], str) and system[key] == "-"):
					row[key] = system[key]
			elif isinstance(system[key], OrderedDict):
				for field in system[key].keys():
					if field in headers:
						if system[key][field] and not system[key][field] == "-":
							row[field] = system[key][field]

	if "EventData" in event:
		event_data = event['EventData']
		if event_data:
			if "Data" in event_data:
				data = event_data['Data']
				if isinstance(data, list):
					for item in data:
						value = ""
						if "#text" in item:
							value = item['#text']

						if item['@Name'] == "ProcessId" and (value.startswith("C:") or value == "LsaRegisterLogonProcess()"):
							print(f"Skipping ProcessId: {value}")
							return None

						if item['@Name'] == "SubjectLogonId" and value.startswith("192.168"):
							print(f"Skipping SubjectLogonId: {value}")
							return None

						if item['@Name'] == "LogonType" and value.startswith("NT "):  # or not isinstance(value, int)): -> is string, test if: 'int(value) raises exception'?
							print(f"Skipping LogonType: {value}")
							return None

						if value == "-":
							value = ""

						if "\n" in value:
							value = value.replace("\n", "")

						if value.startswith("%"):
							value = value.lstrip("%")

						if "\"" in value:
							value = value.replace("\"", "")

						if "'" in value:
							value = value.replace("'", "")

						if "|" in value:
							value = value.replace("|", ";")

						if value.startswith("0x"):
							value = str(int(value, 16))

						row[item['@Name']] = value
				elif isinstance(data, OrderedDict):
					if data['@Name'] in headers:
						if data['#text'] and not data['#text'] == "-":
							value = data['#text']

							if value.startswith("%"):
								value = value.lstrip("%")

							row[data['@Name']] = value
					else:
						print(f"Found Data dict with unknown format: {data}")
				elif data:
					print(f"Unknown format found in EventData.Data: {data}")
			else:
				print(f"Unknown format found for EventData: {event_data}")

	row_str = ""
	first = True
	for header in headers:
		if header in row:
			if first:
				row_str = row[header]
				first = False
			else:
				row_str += SEP + row[header]
		else:
			row_str += SEP

	return row_str


def from_json():
	path = ""  # REDACTED
	vm_path = ""  # REDACTED
	template = r"Security_(.*).json"

	files = []
	for log_file in os.listdir(path):
		match = re.match(template, log_file)
		if match:
			files.append((match.group(1), log_file))

	imported_persons = []
	with open(os.path.join(get_data_path(), "imported_persons.csv"), 'r') as person_file:
		imported_persons = person_file.read().splitlines()

	for (person, log_file) in files:
		if person not in imported_persons:
			with open(os.path.join(path, log_file), 'r') as log:
				write_xml_to_file(person, log, os.path.join("machines", person), post_fix=".csv")

			with open(os.path.join(get_data_path(), "imported_persons.csv"), 'a+') as person_file:
				person_file.write(person + "\n")

	vm_files = []
	for vm_log in os.listdir(vm_path):
		match = re.match(template, vm_log)
		if match:
			vm_files.append((match.group(1), vm_log))

	imported_vms = []
	with open(os.path.join(get_data_path(), "imported_vms.csv"), 'r') as vm_file:
		imported_vms = vm_file.read().splitlines()

	for (vm, vm_log) in vm_files:
		if vm not in imported_vms:
			with open(os.path.join(vm_path, vm_log), 'r') as log:
				write_xml_to_file(vm, log, os.path.join("machines", vm), post_fix=".csv")

			with open(os.path.join(get_data_path(), "imported_vms.csv"), 'a+') as vm_file:
				vm_file.write(vm + "\n")


if __name__ == '__main__':
	from_json()
